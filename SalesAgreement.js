console.log("Sales Agreement");

/* Init Variables */
let salesAgreements = [];
let articles = [];
let tempPrice = 0;
let finalPrice = 0;

/* Sellers */
const sellers = new Array("Tim Van den Broeck", "Ronny De Kempenaar", "Bart Sterkx",
            "Peter Meuleman", "Dieter De Bakker", "Alain Boets", "Bert Blanckaert"); 

/* Customers */
const customers = [
    {
        name: 'Sofie De Bakker',
        email: 'sofie@gmail.com',
        phone: '0477348798',
        address: 'Bakkerstraat 25',
        postalCode: '9200',
        city: 'Dendermonde'
    },
    {
        name: 'Jan De Bleker',
        email: 'jan@telenet.be',
        phone: '0477123456',
        address: 'IJzerenweg 11',
        postalCode: '9130',
        city: 'Beveren'
    },
    {
        name: 'Pieter De Block',
        email: 'pieter@live.com',
        phone: '0471982354',
        address: 'Dorp 2',
        postalCode: '9140',
        city: 'Temse'
    },
    {
        name: 'Tom Devrieze',
        email: 'tom@outlook.com',
        phone: '0474009831',
        address: 'Plezantstraat 7',
        postalCode: '9250',
        city: 'Gent'
    },
    {
        name: 'Johny Tijdgat',
        email: 'johny@hotmail.com',
        phone: '0470112233',
        address: 'Bakkerstraat 125',
        postalCode: '8900',
        city: 'Ieper'
    },
    {
        name: 'Bjorn de Maeschalk',
        email: 'bjorn@telenet.be',
        phone: '0479102290',
        address: 'Ommegangstraat 345',
        postalCode: '9400',
        city: 'Ninove'
    },
    {
        name: 'Nadine De Wilde',
        email: 'nadine@skynet.be',
        phone: '0499067753',
        address: 'Geeststraat 87',
        postalCode: '9290',
        city: 'Berlare'
    },
];

/* Articles */
const clothes = [
    {
        name: 'T-shirt',
        price: 10,
    },
    {
        name: 'Hemd',
        price: 20,
    },
    {
        name: 'Broek',
        price: 50,
    },
    {
        name: 'Das',
        price: 15,
    },
    {
        name: 'Vest',
        price: 25,
    }, {
        name: 'Polo',
        price: 25,
    }, 
    {
        name: 'Houdie',
        price: 45,
    },
    {
        name: 'Sweater',
        price: 30,
    },
];

const shoes = [
    {
        name: 'Geklede schoenen',
        price: 50,
    },
    {
        name: 'Werkschoenen',
        price: 80,
    },
    {
        name: 'Sportschoenen',
        price: 40,
    },
    {
        name: 'Bergschoenen',
        price: 60,
    },
    {
        name: 'Dansschoenen',
        price: 70,
    },
    {
        name: 'Balletschoenen',
        price: 25,
    },
];

const socks = [
    {
        name: 'Grijze sokken',
        price: 15,
    },
    {
        name: 'Groene sokken',
        price: 13,
    },
    {
        name: 'Rode sokken',
        price: 18,
    },
    {
        name: 'Sportsokken',
        price: 12,
    },
    {
        name: 'Kniesokken',
        price: 25,
    },
    {
        name: 'Sneakersokken',
        price: 25,
    },
];

const underwear = [
    {
        name: 'Boxershort',
        price: 25,
    },
    {
        name: 'Slip',
        price: 20,
    },
    {
        name: 'Boyshort',
        price: 50,
    },
    {
        name: 'Tangaslip',
        price: 15,
    },
    {
        name: 'Onderhemd',
        price: 25,
    },
    {
        name: 'Legging',
        price: 30,
    },
];

const hats = [
    {
        name: 'Baseball pet',
        price: 25,
    },
    {
        name: 'Vilten hoed',
        price: 20,
    },
    {
        name: 'Platte pet',
        price: 35,
    },
    {
        name: 'Trucker hoed',
        price: 15,
    },
    {
        name: 'Bolhoed',
        price: 40,
    },
    {
        name: 'Hoge hoed',
        price: 30,
    },
];

const shawl = [
    {
        name: 'Vierkante sjaal',
        price: 25,
    },
    {
        name: 'Kleine vierkante sjaal',
        price: 20,
    },
    {
        name: 'Langwerpige rechthoekige sjaal',
        price: 35,
    },
    {
        name: 'Colsjaal',
        price: 30,
    },
];

/* Generate 110 Sales Agreements */
for(let i = 0; i < 110; i++)
{
    salesAgreements.push({
        timeStamp: getRandomDateBetween('2016-01-01', '2022-12-31'),
        uniqueId: uniqueId(),
        nameSeller: random_item(sellers),
        articles: getRandomArticles(),
        finalPrice: finalPrice,
        customer: random_item(customers)
    })

    articles = [];
    finalPrice = 0;
} 

/* Convert Array to JSON */
const jsonString = JSON.stringify(salesAgreements);

/* Print JSON Result */
console.log(jsonString);

/* Show data in table */
console.table(salesAgreements);

/* Generate random date between 1 january 2016 and 31 december 2022 */
function getRandomDateBetween(start, end) 
{
    start = Date.parse(start);
    end = Date.parse(end);

    var result = new Date(Math.floor(Math.random() * (end - start + 1) + start))
        .toLocaleDateString('nl-nl', {day:'numeric', month:'long', year: 'numeric'});
  
    return result;
}

/* Generate random Articles */
function getRandomArticles()
{
    shuffleArray(clothes);
    shuffleArray(shoes);
    shuffleArray(socks);
    shuffleArray(underwear);
    shuffleArray(hats);
    shuffleArray(shawl);

    let allArticles = new Array(clothes, shoes, socks, underwear, hats, shawl);
    shuffleArray(articles);

    count = getHowManyDifferentArticles(1,6);
    console.log("Count = " + count);

    for(let i = 0; i < count; i++)
    {
        var item = [];
        item = allArticles[i];
      
        var countItems = getHowManyDifferentArticles(1, item.length);
        var amount = 0;
        var price = 0;
      
        for(let i  = 0; i < countItems; i++)
        {
            amount = getHowManyDifferentArticles(1,5);
           
            articles.push({
                name: item[i].name,
                price: item[i].price,
                amount: amount
            })

            price = item[i].price;
            tempPrice = price * amount;
            finalPrice = finalPrice + tempPrice; 
        }
        shuffleArray(articles);
    }

    return articles;
}

/* Get how many different articles, min and max included */
function getHowManyDifferentArticles(min, max) // min and max included 
{ 
    return Math.floor(Math.random() * (max - min + 1) + min)
}

/* Generate random item from array */
function random_item(items)
{
    return items[Math.floor(Math.random()*items.length)];
}

/* Generate unique ID */
function uniqueId()
{
    const dateString = Date.now().toString(36);
    const randomness = Math.random().toString(36).substring(2);
    console.log(dateString + randomness);
    return dateString + randomness;
}

/* Shuffle array */
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}


//Piece of JSON-RESULT

/* [
    {
       "timeStamp":"5 november 2020",
       "uniqueId":"l1umqbml2oijtl4dbbm",
       "nameSeller":"Bart Sterkx",
       "articles":[
          {
             "name":"Boyshort",
             "price":50,
             "amount":3
          },
          {
             "name":"Geklede schoenen",
             "price":50,
             "amount":2
          },
          {
             "name":"Tangaslip",
             "price":15,
             "amount":1
          },
          {
             "name":"Sneakersokken",
             "price":25,
             "amount":5
          },
          {
             "name":"Balletschoenen",
             "price":25,
             "amount":3
          },
          {
             "name":"Groene sokken",
             "price":13,
             "amount":4
          },
          {
             "name":"Grijze sokken",
             "price":15,
             "amount":3
          },
          {
             "name":"Bergschoenen",
             "price":60,
             "amount":2
          },
          {
             "name":"Werkschoenen",
             "price":80,
             "amount":2
          },
          {
             "name":"Vest",
             "price":25,
             "amount":1
          },
          {
             "name":"Dansschoenen",
             "price":70,
             "amount":4
          },
          {
             "name":"Houdie",
             "price":45,
             "amount":2
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":3
          },
          {
             "name":"Vilten hoed",
             "price":20,
             "amount":2
          },
          {
             "name":"Sportschoenen",
             "price":40,
             "amount":2
          }
       ],
       "finalPrice":1447,
       "customer":{
          "name":"Nadine De Wilde",
          "email":"nadine@skynet.be",
          "phone":"0499067753",
          "address":"Geeststraat 87",
          "postalCode":"9290",
          "city":"Berlare"
       }
    },
    {
       "timeStamp":"26 juni 2016",
       "uniqueId":"l1umqbmm1is0nev277w",
       "nameSeller":"Dieter De Bakker",
       "articles":[
          {
             "name":"Houdie",
             "price":45,
             "amount":1
          },
          {
             "name":"Boyshort",
             "price":50,
             "amount":1
          },
          {
             "name":"Sneakersokken",
             "price":25,
             "amount":5
          },
          {
             "name":"Polo",
             "price":25,
             "amount":3
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":2
          },
          {
             "name":"T-shirt",
             "price":10,
             "amount":3
          },
          {
             "name":"Rode sokken",
             "price":18,
             "amount":2
          },
          {
             "name":"Dansschoenen",
             "price":70,
             "amount":1
          },
          {
             "name":"Werkschoenen",
             "price":80,
             "amount":5
          },
          {
             "name":"Tangaslip",
             "price":15,
             "amount":1
          },
          {
             "name":"Onderhemd",
             "price":25,
             "amount":2
          },
          {
             "name":"Balletschoenen",
             "price":25,
             "amount":3
          },
          {
             "name":"Groene sokken",
             "price":13,
             "amount":3
          },
          {
             "name":"Boxershort",
             "price":25,
             "amount":2
          },
          {
             "name":"Legging",
             "price":30,
             "amount":1
          },
          {
             "name":"Slip",
             "price":20,
             "amount":3
          },
          {
             "name":"Grijze sokken",
             "price":15,
             "amount":3
          },
          {
             "name":"Bergschoenen",
             "price":60,
             "amount":1
          },
          {
             "name":"Broek",
             "price":50,
             "amount":5
          },
          {
             "name":"Sportschoenen",
             "price":40,
             "amount":1
          },
          {
             "name":"Geklede schoenen",
             "price":50,
             "amount":4
          },
          {
             "name":"Sportsokken",
             "price":12,
             "amount":4
          }
       ],
       "finalPrice":1853,
       "customer":{
          "name":"Nadine De Wilde",
          "email":"nadine@skynet.be",
          "phone":"0499067753",
          "address":"Geeststraat 87",
          "postalCode":"9290",
          "city":"Berlare"
       }
    },
    {
       "timeStamp":"5 april 2019",
       "uniqueId":"l1umqbmmcmq89cwspia",
       "nameSeller":"Bart Sterkx",
       "articles":[
          {
             "name":"Polo",
             "price":25,
             "amount":4
          }
       ],
       "finalPrice":100,
       "customer":{
          "name":"Pieter De Block",
          "email":"pieter@live.com",
          "phone":"0471982354",
          "address":"Dorp 2",
          "postalCode":"9140",
          "city":"Temse"
       }
    },
    {
       "timeStamp":"7 oktober 2021",
       "uniqueId":"l1umqbmn69d8rn6h9ll",
       "nameSeller":"Peter Meuleman",
       "articles":[
          {
             "name":"Broek",
             "price":50,
             "amount":1
          },
          {
             "name":"Hemd",
             "price":20,
             "amount":4
          },
          {
             "name":"Polo",
             "price":25,
             "amount":4
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":2
          }
       ],
       "finalPrice":290,
       "customer":{
          "name":"Johny Tijdgat",
          "email":"johny@hotmail.com",
          "phone":"0470112233",
          "address":"Bakkerstraat 125",
          "postalCode":"8900",
          "city":"Ieper"
       }
    },
    {
       "timeStamp":"30 juli 2021",
       "uniqueId":"l1umqbmoxkn1eyk55u9",
       "nameSeller":"Alain Boets",
       "articles":[
          {
             "name":"Polo",
             "price":25,
             "amount":5
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":3
          },
          {
             "name":"Das",
             "price":15,
             "amount":3
          },
          {
             "name":"Houdie",
             "price":45,
             "amount":4
          }
       ],
       "finalPrice":440,
       "customer":{
          "name":"Sofie De Bakker",
          "email":"sofie@gmail.com",
          "phone":"0477348798",
          "address":"Bakkerstraat 25",
          "postalCode":"9200",
          "city":"Dendermonde"
       }
    },
    {
       "timeStamp":"13 mei 2022",
       "uniqueId":"l1umqbmotw3ge95yb1",
       "nameSeller":"Alain Boets",
       "articles":[
          {
             "name":"Vest",
             "price":25,
             "amount":2
          },
          {
             "name":"Langwerpige rechthoekige sjaal",
             "price":35,
             "amount":2
          },
          {
             "name":"Boxershort",
             "price":25,
             "amount":5
          },
          {
             "name":"Colsjaal",
             "price":30,
             "amount":2
          },
          {
             "name":"Vilten hoed",
             "price":20,
             "amount":5
          },
          {
             "name":"Onderhemd",
             "price":25,
             "amount":5
          },
          {
             "name":"Boyshort",
             "price":50,
             "amount":4
          },
          {
             "name":"Bergschoenen",
             "price":60,
             "amount":1
          },
          {
             "name":"Hemd",
             "price":20,
             "amount":2
          },
          {
             "name":"Slip",
             "price":20,
             "amount":1
          },
          {
             "name":"Baseball pet",
             "price":25,
             "amount":5
          },
          {
             "name":"Bolhoed",
             "price":40,
             "amount":5
          },
          {
             "name":"Vierkante sjaal",
             "price":25,
             "amount":1
          },
          {
             "name":"Polo",
             "price":25,
             "amount":4
          },
          {
             "name":"Broek",
             "price":50,
             "amount":2
          },
          {
             "name":"Rode sokken",
             "price":18,
             "amount":2
          },
          {
             "name":"Legging",
             "price":30,
             "amount":5
          },
          {
             "name":"Dansschoenen",
             "price":70,
             "amount":2
          },
          {
             "name":"Trucker hoed",
             "price":15,
             "amount":3
          },
          {
             "name":"Sportschoenen",
             "price":40,
             "amount":5
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":3
          },
          {
             "name":"Geklede schoenen",
             "price":50,
             "amount":1
          },
          {
             "name":"Platte pet",
             "price":35,
             "amount":3
          }
       ],
       "finalPrice":2216,
       "customer":{
          "name":"Nadine De Wilde",
          "email":"nadine@skynet.be",
          "phone":"0499067753",
          "address":"Geeststraat 87",
          "postalCode":"9290",
          "city":"Berlare"
       }
    },
    {
       "timeStamp":"30 juli 2021",
       "uniqueId":"l1umqbmp171ijsxa62v",
       "nameSeller":"Bart Sterkx",
       "articles":[
          {
             "name":"Polo",
             "price":25,
             "amount":5
          },
          {
             "name":"Boxershort",
             "price":25,
             "amount":5
          },
          {
             "name":"T-shirt",
             "price":10,
             "amount":4
          },
          {
             "name":"Hemd",
             "price":20,
             "amount":4
          },
          {
             "name":"Geklede schoenen",
             "price":50,
             "amount":2
          },
          {
             "name":"Tangaslip",
             "price":15,
             "amount":2
          },
          {
             "name":"Vest",
             "price":25,
             "amount":1
          },
          {
             "name":"Sportschoenen",
             "price":40,
             "amount":5
          },
          {
             "name":"Sneakersokken",
             "price":25,
             "amount":5
          },
          {
             "name":"Bergschoenen",
             "price":60,
             "amount":2
          },
          {
             "name":"Balletschoenen",
             "price":25,
             "amount":4
          },
          {
             "name":"Dansschoenen",
             "price":70,
             "amount":5
          },
          {
             "name":"Houdie",
             "price":45,
             "amount":1
          },
          {
             "name":"Broek",
             "price":50,
             "amount":1
          }
       ],
       "finalPrice":1515,
       "customer":{
          "name":"Johny Tijdgat",
          "email":"johny@hotmail.com",
          "phone":"0470112233",
          "address":"Bakkerstraat 125",
          "postalCode":"8900",
          "city":"Ieper"
       }
    },
    {
       "timeStamp":"22 januari 2020",
       "uniqueId":"l1umqbmq3fjc7bmoz9t",
       "nameSeller":"Dieter De Bakker",
       "articles":[
          {
             "name":"Geklede schoenen",
             "price":50,
             "amount":2
          },
          {
             "name":"Grijze sokken",
             "price":15,
             "amount":4
          },
          {
             "name":"Rode sokken",
             "price":18,
             "amount":5
          },
          {
             "name":"Slip",
             "price":20,
             "amount":3
          },
          {
             "name":"Bergschoenen",
             "price":60,
             "amount":5
          },
          {
             "name":"T-shirt",
             "price":10,
             "amount":4
          },
          {
             "name":"Groene sokken",
             "price":13,
             "amount":3
          },
          {
             "name":"Houdie",
             "price":45,
             "amount":3
          },
          {
             "name":"Onderhemd",
             "price":25,
             "amount":5
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":5
          },
          {
             "name":"Vest",
             "price":25,
             "amount":4
          },
          {
             "name":"Sportschoenen",
             "price":40,
             "amount":3
          },
          {
             "name":"Polo",
             "price":25,
             "amount":2
          },
          {
             "name":"Hemd",
             "price":20,
             "amount":1
          },
          {
             "name":"Broek",
             "price":50,
             "amount":5
          },
          {
             "name":"Balletschoenen",
             "price":25,
             "amount":5
          },
          {
             "name":"Das",
             "price":15,
             "amount":2
          }
       ],
       "finalPrice":1794,
       "customer":{
          "name":"Johny Tijdgat",
          "email":"johny@hotmail.com",
          "phone":"0470112233",
          "address":"Bakkerstraat 125",
          "postalCode":"8900",
          "city":"Ieper"
       }
    },
    {
       "timeStamp":"25 september 2016",
       "uniqueId":"l1umqbmrrt2ijnxs6jo",
       "nameSeller":"Peter Meuleman",
       "articles":[
          {
             "name":"Hemd",
             "price":20,
             "amount":2
          },
          {
             "name":"T-shirt",
             "price":10,
             "amount":2
          },
          {
             "name":"Sweater",
             "price":30,
             "amount":5
          },
          {
             "name":"Vest",
             "price":25,
             "amount":5
          },
          {
             "name":"Broek",
             "price":50,
             "amount":2
          }
       ],
       "finalPrice":435,
       "customer":{
          "name":"Tom Devrieze",
          "email":"tom@outlook.com",
          "phone":"0474009831",
          "address":"Plezantstraat 7",
          "postalCode":"9250",
          "city":"Gent"
       }
    },
    {
       "timeStamp":"19 februari 2017",
       "uniqueId":"l1umqbmsaypysf9lis6",
       "nameSeller":"Dieter De Bakker",
       "articles":[
          {
             "name":"Vest",
             "price":25,
             "amount":2
          },
          {
             "name":"Hemd",
             "price":20,
             "amount":4
          }
       ],
       "finalPrice":130,
       "customer":{
          "name":"Pieter De Block",
          "email":"pieter@live.com",
          "phone":"0471982354",
          "address":"Dorp 2",
          "postalCode":"9140",
          "city":"Temse"
       }
    }
 ] */